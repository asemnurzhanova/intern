<?php
require 'db.php';
  require('libs/phpQuery.php');
  define('HOST','https://www.worldometers.info/coronavirus/');

  R::wipe('corona');

  $data_site = file_get_contents(HOST);
  $document = phpQuery::newDocument($data_site);
  $ccorona = $document->find('table')->eq(1)->find('tbody tr');

  foreach ($ccorona as $el) {
  $pq = pq($el);
  $country = $pq->find('td')->eq(1)->text();
  $cases = $pq->find('td')->eq(2)->text();
  $deaths = $pq->find('td')->eq(4)->text();
  $recovered = $pq->find('td')->eq(6)->text();

  $corona = R::dispense('corona');
      if(!empty($country)) $corona->country = strip_tags($country);
      if(!empty($cases)) $corona->cases = strip_tags($cases);
      if(!empty($deaths)) $corona->deaths = strip_tags($deaths);
      if(!empty($recovered)) $corona->recovered = strip_tags($recovered);
      R::store($corona);
}
$ids =  R::getCol( 'SELECT id FROM corona' );
$random_index = rand(9, sizeof($ids)-9);
$random_name = R::load( 'corona', $ids[$random_index] );
?>
<html>
<head>
    <title>Covid-19</title>
</head>
<body>
<table>
    <tr>
        <th>Страна</th>
        <th>Заболели</th>
        <th>Умерло</th>
        <th>Выздоровели</th>
    </tr>
    <tr>
        <td> <?php echo $random_name->country;?> </td>
        <td> <?php echo $random_name->cases;?> </td>
        <td> <?php echo $random_name->deaths;?> </td>
        <td> <?php echo $random_name->recovered;?> </td>
    </tr>
</table>

</body>
</html>